#ifndef _KISS_FFTR_GUTS_H
#define _KISS_FFTR_GUTS_H

#include "_kiss_fft_guts.h"
#include "kiss_fft.h"

struct kiss_fftr_state{
    kiss_fft_cfg substate;
    kiss_fft_cpx * tmpbuf;
    kiss_fft_cpx * super_twiddles;
#ifdef USE_SIMD
    void * pad;
#endif
};

#endif // _KISS_FFTR_GUTS_H
